import { Component, OnInit } from '@angular/core';
import { GlobalVariable } from './globals';
import { SlimScrollOptions } from 'ng2-slimscroll';

@Component({
  selector: 'app-new-timeline',
  template: `
<div class="card">
  <div style=" position: relative;">
    <div style="margin: 0 37px 5px;overflow: hidden;" >
      <div style=" float: left; width: 33.33333%; min-height: 1px;  padding-right: 3px;  padding-left: 3px; ">
        <div style=" height: 350px;display: block;" >
          <div style="padding-right: 0px;background: white;margin: 0; height: 100%;overflow-y: auto;   padding: 0px 0; border-left: none; border-right: none;border-color: #ddd; font-size: 13px;"  slimScroll [options]="options">
            <div *ngFor="let data of itemsDay1" >
              <span style="position: relative; display:block;padding:0.75rem 1.25rem;background-color:#fff;border:1px solid #ddd;">
                     <a>
                   <strong style ="color:#000" [innerHTML]="data.title"></strong>
                </a>
                <p>
                  <small>
                      <span  style="color: #666;">{{data.media_displayName}}</span>
                  </small>
              </p>
              </span>
            </div>
          </div>
        </div>
      </div>

      <div style=" float: left; width: 33.33333%; min-height: 1px;  padding-right: 3px;  padding-left: 3px; ">
        <div style=" height: 350px;display: block;" >
          <div style="padding-right: 0px;background: white;margin: 0; height: 100%;overflow-y: auto;   padding: 0px 0; border-left: none; border-right: none;border-color: #ddd;
                font-size: 13px;"  slimScroll [options]="options">
            <div *ngFor="let data of itemsDay2">
              <span style="position: relative; display:block;padding:0.75rem 1.25rem;background-color:#fff;border:1px solid #ddd;">
                     <a>
                    <strong style ="color:#000" [innerHTML]="data.title"></strong>
                </a>
                <p>
                  <small>
                      <span style="color: #666;">{{data.media_displayName}}</span>
                  </small>
              </p>
              </span>
            </div>
          </div>
        </div>
      </div>

      <div style=" float: left; width: 33.33333%; min-height: 1px;  padding-right: 3px;  padding-left: 3px; " >
        <div style=" height: 350px;display: block;"  >
          <div style="padding-right: 0px;background: white;margin: 0; height: 100%;overflow-y: auto;   padding: 0px 0; border-left: none; border-right: none;border-color: #ddd;
                font-size: 13px;"  slimScroll [options]="options">
            <div *ngFor="let data of itemsDay3" >
              <span style="position: relative; display:block;padding:0.75rem 1.25rem;background-color:#fff;border:1px solid #ddd;">
                    <a>
                  <strong style ="color:#000" [innerHTML]="data.title"></strong>
                </a>
                <p>
                  <small>
                      <span  style="color: #666;">{{data.media_displayName}}</span>
                  </small>
              </p>
              </span>
            </div>
          </div>
        </div>
      </div>


    </div>
    <div style="margin: 0 37px;">
      <!--<div class="row-timeline-page">-->
      <div style="display: table;table-layout: fixed;width: 100%; border-left: 1px solid #515151; border-right: 1px solid #515151;">
        <div style="border-left: 1px solid #515151;border-right: 1px solid #515151;display: table-cell;font-weight: bold;line-height: 30px; padding: 0 10px;vertical-align: middle; float: left; width: 33.33333%;" class=" text-primary"  *ngFor="let date of items; let ind = index">{{date.text}}</div>
        <!--<div class="cell-timeline-page text-primary col-md-4" *ngFor="let date of items; let ind = index">{{date.text}}</div>-->
      </div>
    </div>
    <div style="left: 0; position: absolute;bottom: 0;">
      <span style="cursor:pointer" id="prev-timeday" (click)="prevDay($event)">
            <img height="27%" width="25%" style="float: left" src = 'assets/img/left.png' > 
        </span>
    </div>
    <div style="right: 0; position: absolute;bottom: 0;">
      <span style="cursor:pointer" id="next-timeday" (click)="nextDay($event)">
            <img height="27%" width="25%" style="float: right" src = 'assets/img/right.png' > 
      </span>
    </div>
  </div>
</div>`,
  styles: ['  html {box-sizing: border-box; }*, *::before, *::after {  box-sizing: inherit; } body { background-color: #e4e5e6; } a {color: #20a8d8; text-decoration: none; } a:focus, a:hover {color: #167495; text-decoration: underline; } a:focus {outline: 5px auto -webkit-focus-ring-color; outline-offset: -2px; } .card { position: relative; display: block; margin-bottom: 0.75rem; background-color: #e4e5e6;  border: 1px solid #e4e5e6; }']

})
export class BaseWidgetNewTimelineComponent implements OnInit {
  options: SlimScrollOptions;
  private dates:any;
  private date:any;
  private prevDate:any;
  private nextDate;
  public date1;
  public date2;
  public date3;
  private days:any;
  private items: any;

  private itemsDay1: any;
  private itemsDay2: any;
  private itemsDay3: any;

  private splitDays1: any;
  private splitDays2: any;
  private splitDays3: any;

  private tgl1 ="20170423";
  private tgl2="20170424";
  private tgl3 ="20170425";

  private hari1:any;
  private hari2:any;
  private hari3:any;

  public timeline = {
       "20170423": {
         "items1":[
       {
      "id": "2282182953410560",
      "title": "Months into the Trump administration, the State Department still doesn&#39;t have a new spokesperson",
      "media_displayName": "Business Insider Australia",
      "img": "",
      "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
      "date": "2017-03-29 22:55:57+0800",
      "content": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, and the acting spokesperson, Mark Toner, hasn&#8217;t been holding regular on-camera press briefings, The Wall Street Journal noted. There weren&#8217;t any on-camera briefings for the first six weeks of the Trump administration, and they have stopped soon after they started again at the beginning of the month. The State Department is holding background briefings via phone, but it&#8217;s still a significant change from the near-daily briefings under the Obama administration. The lack of a press person has posed problems outside of just a lack of press briefings. Rex Tillerson made his first trip to Asia earlier this month as secretary of state, and there was quite a bit of confusion as the press scrambled to keep up with him and get enough access to report what was going on. Much of the coverage of the trip focused on how little access journalists who report on the State Department had. Members of the press were vocally complaining because only one reporter, from the conservative Independent Journal Review, travelled with Tillerson. Before the trip, Toner attempted to defend the State Department&#8217;s decision to only bring one reporter, but his explanations fell a bit flat as he admitted to reporters that the press arrangement on the Asia trip was an \"out-of-the-box decision.\" The Trump administration has reportedly tapped a new spokesperson for the State Department &#8212; Fox News anchor Heather Nauert &#8212; but her position isn&#8217;t yet official, and she is waiting for her security clearance to be approved, according to the Journal.",
      "link": "https://www.businessinsider.com.au/state-department-spokesperson-2017-3",
      "timestamp": 1490802957000
      },
      {
      "id": "2291247154728960",
      "title": "North Korea says ready to strike US aircraft carrier",
      "media_displayName": "Global Times",
      "img": "",
      "text": "North Korea said on Sunday it was ready to sink a US aircraft carrier to demonstrate its military might, as two Japanese navy ships joined a US carrier group for exercises in the western ...",
      "date": "2017-04-23 22:58:39+0800",
      "content": "North Korea said on Sunday it was ready to sink a US aircraft carrier to demonstrate its military might, as two Japanese navy ships joined a US carrier group for exercises in the western Pacific.  US President Donald Trump ordered the USS Carl Vinson carrier strike group to sail to waters off the Korean peninsula in response to rising tension over the North&#39;s nuclear and missile tests, and its threats to attack the United States and its Asian allies.  The US has not specified where the carrier strike group is as it approaches the area. US Vice President Mike Pence said on Saturday it would arrive \"within days\" but gave no other details.  North Korea remained defiant.  \"Our revolutionary forces are combat-ready to sink a US nuclear-powered aircraft carrier with a single strike,\" the Rodong Sinmun, the newspaper of the North&#39;s ruling Workers&#39; Party, said in a commentary.  The paper likened the aircraft carrier to a \"gross animal\" and said a strike on it would be \"an actual example to show our military&#39;s force.\"  The commentary was carried on page three of the newspaper, after a two-page feature about leader Kim Jong-un inspecting a pig farm.  North Korea detained a Korean-American man in his fifties on Friday, South Korea&#39;s Yonhapnews agency reported, bringing the total number of US citizens held by Pyongyang to three.  The man, identified only by his surname Kim, had been in North Korea for a month to discuss relief activities, Yonhap said. He was arrested at Pyongyang International Airport on his way out of the country.  North Korea has conducted five nuclear tests, two of them last year, and is working to develop nuclear-tipped missiles can reach the US.",
      "link": "http://www.globaltimes.cn/content/1043739.shtml",
      "timestamp": 1492963119000
      },
      {
      "id": "2291270324068352",
      "title": "U.S. Homeland Security not targeting Dreamers - Kelly | Reuters",
      "media_displayName": "First Post",
      "img": "http://s2.firstpost.in/wp-content/uploads/2011/04/blank_dummy_image.jpg",
      "text": "WASHINGTON The Department of Homeland Security will not target immigrants brought to the United States as children for deportation, despite conflicting statements within the Trump administration, ...",
      "date": "2017-04-23 22:09:19+0800",
      "content": "WASHINGTON The Department of Homeland Security will not target immigrants brought to the United States as children for deportation, despite conflicting statements within the Trump administration, its secretary John Kelly said on Sunday.Kelly, asked on Sunday morning talk shows to clarify the department&#39;s position on the status of these illegal immigrants protected under an Obama-era program, said the agency is focused on deporting only dangerous criminals. \"My organization has not targeted these so-called Dreamers,\" Kelly told CNN, referring to the name given to those granted protections under the Deferred Action for Childhood Arrivals (DACA) program created by Democratic President Barack Obama and extended by Republican President Donald Trump.\"We have many, many more important criminals to go after,\" he said. Trump has said Dreamers \"have nothing to worry about,\" but Attorney General Jeff Sessions last week said immigrants who arrived in the United States as children were \"subject to being deported.\"On Sunday, Sessions walked back his earlier statement. \"I believe that everyone that enters the country unlawfully is subject to being deported; however, we&#39;ve got -- we don&#39;t have the ability to round up everybody and there&#39;s no plans to do that,\" Sessions said on ABC. \"But we&#39;re going to focus first, as the president has directed us, on the criminal element.\" On Feb. 17, Juan Manuel Montes, 23, who had lived in the United States since he was 9, was deported from the border city of Calexico, California, after being questioned by a U.S. Customs and Border Protection (CBP) officer. That was the first documented deportation of a Dreamer.Kelly said in another Sunday interview on CBS that while Dreamers are not being targeted, several of them end up detained by immigration officers as they round up criminals.\"People fall into our hands incidentally that we have no choice in most cases but to go ahead and put in the system,\" he said.",
      "link": "http://www.firstpost.com/world/u-s-homeland-security-not-targeting-dreamers-kelly-reuters-3400488.html",
      "timestamp": 1492960159000
      },
      {
      "id": "2291270324068353",
      "title": "Japan&#39;s Aso pushes back on U.S. call for scrutiny of currency moves | Reuters",
      "media_displayName": "First Post",
      "img": "http://s2.firstpost.in/wp-content/uploads/2017/04/8693220x220_Watermark.jpg",
      "text": "WASHINGTON Japanese Finance Minister Taro Aso said on Saturday trade imbalances cannot be fixed through exchange-rate adjustments alone, pushing back against Washington&#39;s calls ...",
      "date": "2017-04-23 22:06:55+0800",
      "content": "WASHINGTON Japanese Finance Minister Taro Aso said on Saturday trade imbalances cannot be fixed through exchange-rate adjustments alone, pushing back against Washington&#39;s calls to have more rigorous IMF scrutiny of currency moves.Earlier, U.S. Treasury Secretary Steven Mnuchin called on the International Monetary Fund to enhance surveillance of its members&#39; exchange rates and external imbalances, as large trade imbalances would hamper \"free and fair\" trade.But Aso told the IMF&#39;s steering committee there were limits to using exchange-rate assessments to address current account imbalances for a country like Japan. That is because the recent increases in Japan&#39;s current account surplus are driven largely by rising dividend payments and repatriation of revenues from overseas investments, instead of any boost to exports from a weak yen.\"In cases where &#39;excessive&#39; imbalances exist, they should be addressed by a package of macroeconomic and structural policy measures,\" Aso said in a speech to the International Monetary and Financial Committee. \"Adjustment through changes in the exchange rate is not necessarily required,\" he said.U.S. President Donald Trump has criticized countries like Japan, Germany and China for running large trade surpluses with the United States and weakening their currencies to gain an unfair trade advantage. Japanese policymakers fear the Trump administration may accuse the Bank of Japan of using ultra-loose monetary policy to weaken the yen and bind Tokyo&#39;s hands on currency intervention to address any unwelcome spike in the yen.\"With downside risks and uncertainty persisting, the stability of financial and exchange rate markets is especially important,\" Aso said.\"Excess volatility and disorderly movements in exchange rates can have adverse implications for economic and financial stability,\" he added, referring to language in the G20 agreement that Tokyo cites as giving it room to intervene in the currency market to stem sharp yen gains.",
      "link": "http://www.firstpost.com/fwire/japans-aso-pushes-back-on-u-s-call-for-scrutiny-of-currency-moves-reuters-3400472.html",
      "timestamp": 1492960015000
      },
      ]
       },

    
      "20170424":{
         "items1":[
      {
      "id": "2291605054693377",
      "title": "John Oliver&#58; Why Ivanka and Jared Kushner&#39;s influence on Donald Trump isn&#39;t what you think",
      "media_displayName": "Business Insider Australia",
      "img": "https://static.businessinsider.com/image/58fe005a0ba0b81c008b5763-1200/image.jpg",
      "text": "On Sunday, &#8220;Last Week Tonight with John Oliver&#8221; had a 22-minute segment delving into the qualifications of Ivanka Trump, daughter of President Donald Trump, and Jared Kushner, ...",
      "date": "2017-04-24 22:56:29+0800",
      "content": "On Sunday, \"Last Week Tonight with John Oliver\" had a 22-minute segment delving into the qualifications of Ivanka Trump, daughter of President Donald Trump, and Jared Kushner, her husband, for working at the White House. Oliver points out that for liberals, the popular assumption is that both will be a moderating influence on President Trump. But is that really the case? The host of the HBO show broke down their roles&#58; Ivanka is an unpaid \"assistant to the president,\" while Kushner pretty much has his hand in everything, including being responsible for brokering peace in the Middle East and revamping the entire federal government. \"It is not unusual for powerful men to give their son-in-laws do-nothing jobs, but leave it to Donald Trump, who can&#8217;t even get nepotism right, to give his a do-everything job,\" Oliver said. But is this all just window dressing? Though Ivanka has been a staple in Trump&#8217;s businesses and TV shows since she was a teen, her involvement since Trump&#8217;s candidacy doesn&#8217;t seem to have shifted Trump&#8217;s thoughts, Oliver argues. Ivanka was reportedly the one who got her father to meet with Al Gore at Trump Tower, but three days later, Trump named climate change denier Scott Pruitt as head of the EPA. And issues she&#8217;s gotten behind, like family leave and childcare, don&#8217;t seem to have influenced Trump. His proposals would give families who have incomes between $US10,000 and $US30,000 average annual benefits of just $US10 a year. Though Ivanka is perceived to be the opposite of her father, Oliver found a passage from her 2009 book \"The Trump Card\" that might make you think otherwise. In one section she wrote, \"Don&#8217;t go out of your way to correct a false assumption if it plays to your advantage.\" \"She&#8217;s pretty much telling you to your face not to trust any assumption you are making about her,\" Oliver said. \"It is possible she is doing nothing to moderate her father.\" Kushner, on the other hand, doesn&#8217;t seem to have a glowing business resume. He bought a swanky Manhattan building for a record $US1.8 billion, but 10 years later the building is struggling with debt payments. However, he&#8217;s the primary point of contact for two dozen different countries. Those who know Kushner have praised his listening skills. \"So hold on, he&#8217;s brilliant because he&#8217;s quiet?\" Oliver asked. \"Just because you don&#8217;t talk does not necessarily mean you&#8217;re thinking something amazing.\" Oliver also touched on the fact that many people have no idea what Kushner&#8217;s voice sounds like. So he dug up an old interview with Kushner, but when he played it, the clip actually had comedian Gilbert Gottfried speaking over Kushner with his trademark high-pitched voice. \"This may seem like an evisceration of Jared and Ivanka, but it&#8217;s really not,\" Oliver said. \"I don&#8217;t know enough about them to eviscerate them, just as you don&#8217;t know enough about them to justify putting any real hopes in them. Because it is dangerous to think of them as a moderating influence, as reassuring as that may feel.\" Oliver added a warning about Ivanka and Jared for liberals&#58; \"If they are the reason you are sleeping at night, you should probably still be awake.\" Watch Oliver&#8217;s entire segment on Ivanka and Jared below&#58;",
      "link": "https://www.businessinsider.com.au/john-oliver-ivanka-trump-jared-kushner-2017-4",
      "timestamp": 1493049389000
      },
          {
      "id": "2291609571954688",
      "title": "Trump to sign new orders on environment",
      "media_displayName": "Global Times",
      "img": "",
      "text": "US President Donald Trump this week will sign new executive orders before he completes his first 100 days in office, including two on energy and the environment, which would make it ...",
      "date": "2017-04-24 22:53:39+0800",
      "content": "US President Donald Trump this week will sign new executive orders before he completes his first 100 days in office, including two on energy and the environment, which would make it easier for the US to develop energy on and offshore, a White House official said on Sunday.  \"This builds on previous executive actions that have cleared the way for job-creating pipelines, innovations in energy production, and reduced unnecessary burden on energy producers,\" the official said on condition of anonymity.  On Wednesday, Trump is expected to sign an executive order related to the 1906 Antiquities Act, which enables the president to designate federal areas of land and water as national monuments to protect them from drilling, mining and development, the source said.  On Friday, Trump is expected to sign an order to review areas available for offshore oil and gas exploration, as well as rules governing offshore drilling.  The new measures would build on a number of energy- and environment-related executive orders signed by Trump seeking to gut most of the climate change regulations put in place by predecessor president Barack Obama.  A summary of the forthcoming orders, seen by Reuters, say past administrations \"overused\" the Antiquities Act, putting more federal areas under protection than necessary. Obama had used the Antiquities Act more than any other president, his White House said in December, when he designated over 647 497 hectares of land in Utah and Nevada as national monuments, protecting two areas rich in Native American artifacts from mining, oil and gas drilling.  The summary also says previous administrations have been \"overly restrictive\" of offshore drilling.  Late in Obama&#39;s second term, he banned new drilling in federal waters in parts of the Atlantic and Arctic oceans using a 1950s-era law that environmental groups say would require a drawn out court challenge to reverse. Interior Secretary Ryan Zinke said during his January confirmation hearing that Trump could \"amend\" Obama&#39;s monument designations but any move to rescind a designation would be challenged.  Last month, Trump signed an order calling for a review of Obama&#39;s Clean Power Plan.  Trump is also expected to sign an order to create an office of accountability in the Veterans Affairs department.",
      "link": "http://www.globaltimes.cn/content/1043907.shtml",
      "timestamp": 1493049219000
      },
      {
      "id": "2291604031287296",
      "title": "Macrons victory averts fears of euroskeptic-only runoff European stocks surge",
      "media_displayName": "Big News Network",
      "img": "http://cdn.bignewsnetwork.com/cus1493045021.jpg",
      "text": "Centrist Emmanuel Macron&#8217;s victory in the first round of the French Presidential election has averted fears of a euroskeptic-only runoff sending European stocks surging. On Sunday, ...",
      "date": "2017-04-24 22:52:16+0800",
      "content": "Centrist Emmanuel Macron&#8217;s victory in the first round of the French Presidential election has averted fears of a euroskeptic-only runoff sending European stocks surging. On Sunday, Macron won the first round of the French presidential voting with 23.9 percent of the vote, ahead of far-right euroskeptic candidate Marine Le Pen with 21.4 percent. The two are now set to face off in the final round of the election on May 7. A poll conducted late on Sunday from Ipsos/Sopra Steria showed that Macron would likely win that runoff by 62 percent to 38 percent. Meanwhile, defeated conservative Francois Fillon and Socialist Benoit Hamon, the other two mainstream candidates defeated in the first round have thrown their support behind Macron. Le Pen has called for scrapping the euro and exiting the European Union, a view that European leaders are not very happy about. Ipek Ozkardeskaya, senior market analyst at LCG said in a note to clients, \"Her radical views on immigration, the European Union and the euro should see a solid resistance from the majority of the population.\" European stocks surged on the news of Macron&#8217;s victory - with the French CAC 40 rose 4.5 percent and is headed for its highest close since January 2008. The Stoxx Europe 600 index meanwhile climbed 2 percent, and Germany&#8217;s DAX 30 index was up 3 percent. The euro too gained 1.2 percent against the dollar, hovering at five-month highs. The Nikkei 225 climbed nearly 1.4 percent, as investors backed away from perceived havens such as the Japanese yen. Pascale Auclair, global head of investment at La Francaise Asset Management has said, \"The risk of a europhobic president has been lifted for markets. I think there will be a period of relief.\" Shares in French companies and banks recorded the biggest gains across the region with the euro zone&#39;s bank index rising over 5 percent to its highest level in 16 months. Paris blue chips hit their highest since April 2015. UniCredit analysts said in a note, \"French banks alongside European banks will likely be the biggest winners.\" On Friday, Gold prices climbed amid investor uncertainty ahead of the French election. Gold tumbled $16.60 or 1.3 percent to $1,272.50 an ounce. Dow Jones Industrial Average futures shot up 204 points, or 1 percent, to 20,704 on relief over the French vote. S&amp;P 500 futures surged 26.35 points, or 1.1 percent, to 2,373.75. Nasdaq-100 futures jumped 56.50 points, or 1 percent, to 5,498.50. Yields for the 10-year Treasury jumped to 2.29 percent from 2.23 percent late Friday. The dollar shot to over 110 yen from a level of 109 yen late Friday in New York. On Monday, U.S. stocks were set to rip higher and perceived haven assets gold and the yen fell, after Macron&#8217;s victory in the first round. Major markets showed gains for the week, with the Dow rising 0.5 percent, the S&amp;P 500 advanced 0.9 percent, and the Nasdaq gained 1.8 percent, on the heels of two weeks of declines. Now, investors have set a hawk eye on a \"massive\" U.S. tax package that President Donald Trump is set to announce on Wednesday.",
      "link": "http://www.bignewsnetwork.com/news/252927256/macrons-victory-averts-fears-of-euroskeptic-only-runoff-european-stocks-surge",
      "timestamp": 1493049136000
      },
      {
      "id": "2291600566792192",
      "title": "Barbra Streisand at 75&#58; A girl from Brooklyn makes it big",
      "media_displayName": "Masress",
      "img": "http://images1.masress.com/en/dailynews/en/dailynews",
      "text": "She won over both Hollywood and Broadway, amassing a loyal following among fans as she did so. Even at 75, Barbra Streisand still sees herself as the girl next door.Barbra Streisand ...",
      "date": "2017-04-24 22:41:16+0800",
      "content": "She won over both Hollywood and Broadway, amassing a loyal following among fans as she did so. Even at 75, Barbra Streisand still sees herself as the girl next door.Barbra Streisand has achieved what most people can only dream of. She has celebrated worldwide success and recognition for more than 50 years &#8211; winning all the awards imaginable along the way, including Oscars, Emmy, Golden Globes and Tonys. She has sold more than 140 million albums &#8211; more than The Beatles. Yet sometimes the girl she once was appears. Born into an impoverished Polish-Russian Orthodox Jewish family in Williamsburg, Brooklyn, Streisand still has problems with spending money on frivolous things. As she told Oprah Winfrey, the Brooklyn part of her still has her wondering if a tile really is worth its $10.95 price tage.Everything but the &#8216;ugly duckling&#39;The native New Yorker appears friendly, even with fans, breaking with the clich&#233; of the distant diva. She achieved her fame in spite of being labeled an \"ugly duckling\" in her youth due to a large nose and squint. But she never considered plastic surgery to alter her appearance, claiming she was afraid of the pain. \"And how could I trust the doctor&#39;s aesthetic taste? How would I know that he wouldn&#39;t take away too much?\"The \"flaws\" in her appearance seemed to make her all the more attractive to fans. She also won them over with her confession that she suffered from stage fright and had to control it with medication. In this regard, Streisand says, she gives off the appearance of being just like everyone else.&#8216;Funny Girl&#39; instead of ShakespeareWhen she was just 15 months old, Streisand&#39;s father died. Her mother, a singer, set aside her dreams to support the family by taking on a job as a school secretary. Barbra continued to dream, however, signing up for acting classes at the age of 14. At the same time, she sang in the school chorus and later in small nightclubs. \"Because I was known as &#8216;the kid on the block with a good voice&#39; I entered a talent competition, which I thought might at least help pay for my meals until I could do Shakespeare or Ibsen,\" she told the British newspaper, \"Telegraph.\"She won first place in the competition and her star began to rise. Her career began not on the theater stage but in big-name nightclubs, where Streisand worked as a singer. At 19, she made her Broadway debut and just two years later, she won the critic&#39;s prize for the best actress in a musical for her role in \"I Can Get It for You Wholesale.\"Nearly simultaneously, her eponymous album was released and won two Grammy awards. In 1968, she made her big breakthrough in the musical, \"Funny Girl.\" It tells the story of the Jewish comedian Fanny Brice who overcame the naysayers who&#39;d labeled her an ugly duckling and became a Broadway star in 1910. The role seemed to be perfectly suited to Barbra.Hollywood careerAwarded an Oscar for her performance in the musical, the then-25-year-old continued to appear on the silver screen in films like \"What&#39;s Up, Doc?\" (1972) and \"The Way We Were\" (1973) and carried her fame with her to cameo roles on television series, including \"Glee\" and \"Dancing with the Stars.\" Her repertoire shows the wide spectrum of her talent, as she appears in comedies and dramas alike. She has appeared opposite Omar Sharif, Yves Montand, Sidney Poitier, Nick Nolte, Dustin Hoffman and Robert de Niro.Her favorite leading man, however, was Robert Redford in the romance \"The Way We Were.\" \"We never quite knew what the other one was going to do so we were watching each other carefully, interested in each other, and I think the audience felt that.\"In 1983, Streisand took on her first role as producer, director, main actress and singer all at once in the film \"Yentl.\" The movie centers on a young Jewish woman who dresses as a boy in order to attend Yeshiva, a Jewish school, to study the Talmud.\"I don&#39;t want to brag,\" Streisand told the press at the time, \"But Steven Spielberg said that he would have liked to have criticized &#8216;Yentl&#39; but in his eyes, besides &#8216;Citizen Kane,&#39; there was no better film.\"\"Yentl\" went on to win an Oscar for best soundtrack. Her later directorial success, \"The Prince of Tides\" (1991), was nominated for seven Oscars. She plays the main character opposite Nick Nolte; her son Jason Gould, from her first marriage to Elliot Gould, likewise appears in the film.&#8216;A voice like diamonds&#39;Streisand sings in most of her movies &#8211; and boy can she sing! \"I thank God for giving me this great voice,\" she once said. When she was honored with the \"Presidential Medal of Freedom\" in 2015, the highest honor for US civilians, then-US President Barack Obama lauded her, saying she had a voice like diamonds. Many of her songs, like \"Woman In Love,\" \"The Way We Were,\" \"A Star Is Born\" and \"Guilty,\" are evergreen hits.Although she has turned 75 years old, Streisand is a long way off from quitting show business. She just released two new albums and will give two concerts in New York next month. She&#39;s also set to head the new Cultural Center at the World Trade Center in New York beginning in 2020.Streisand is also very politically active, blogging for \"Huffington Post,\" supporting environmental activism and tweeting her uncompromisingly liberal opinions. She has spoken out against Donald Trump and took part in the Women&#39;s March in January of this year.When Barbra has time among all these other activities, she returns to Malibu, California, where she lives with her second husband, actor James Brolin. Happy Birthday, Babs!",
      "link": "http://www.masress.com/en/dailynews/622995",
      "timestamp": 1493048476000
      },
      ]
      },
      

      "20170425": {
         "items1":[
      {
      "id": "2291866259165185",
      "title": "Trump&#39;s &#39;Buy American&#39; policy is getting support from an unlikely place&#58; A Chinese e-commerce giant",
      "media_displayName": "Cnbc",
      "img": "http://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2017/01/26/104244070-GettyImages-631334794.530x298.jpg?v=1485479017",
      "text": "President Donald Trump&#39;s  Buy American  policy is getting support from an unlikely place&#58; China&#39;s biggest e-commerce company. In a video posted Tuesday, Alibaba founder ...",
      "date": "2017-04-25 16:17:20+0800",
      "content": "President Donald Trump&#39;s \"Buy American\" policy is getting support from an unlikely place&#58; China&#39;s biggest e-commerce company. In a video posted Tuesday, Alibaba founder Jack Ma said&#58; \"As incomes rise, Chinese consumers are increasingly looking for high quality products from around the world, especially the United States.\" Ma noted that nearly half a billion people shop on Alibaba&#39;s marketplaces and that by using the platform, American small businesses have the chance to \"grow globally &#8212; like the big guys.\" Now, Alibaba has announced that it will host its first small business event in the U.S. this summer to try to convince American entrepreneurs that the company is on their side &#8212; and can help them reach millions of Chinese consumers. The inaugural conference, \"Gateway &#39;17\" will be held in Detroit, Michigan in June and the company is expecting more than 1,000 businesses from across the nation. While touting China&#39;s growing opportunities, Alibaba will also have to convince attendees that their original businesses and ideas are safe from counterfeiters. Alibaba has an American PR problem&#58; Last December, it landed back on the U.S. government&#39;s \"notorious markets\" list for fakes &#8212; a designation that hurts its image among American businesses. That&#39;s especially important for small businesses that sell original creations on e-commerce platforms like Amazon and Etsy. Alibaba has said it was disappointed by the decision and called for harsher penalties for counterfeiters. Ma even made a personal visit to Trump Tower shortly after the election and vowed to create a million new jobs in the U.S. in the next five years.",
      "link": "http://www.cnbc.com/2017/04/25/trumps-buy-american-policy-is-getting-support-from-an-unlikely-place-a-chinese-e-commerce-giant.html",
      "timestamp": 1493111840000
      },
      {
      "id": "2291864803741698",
      "title": "Qatar air adds San Francisco after emirates&#8217; Trump cuts",
      "media_displayName": "The Malaysian Reserve",
      "img": "",
      "text": "Qatar Airways Ltd announced plans to add San Francisco to its network of US destinations, plotting expansion just days after rival Emirates moved to reduce flights to the country, citing ...",
      "date": "2017-04-25 16:11:15+0800",
      "content": "Qatar Airways Ltd announced plans to add San Francisco to its network of US destinations, plotting expansion just days after rival Emirates moved to reduce flights to the country, citing President Donald Trump&#8217;s travel restrictions. The Doha-based carrier will serve the California city starting in 2018, the company said yesterday at a press conference in Dubai. The Persian Gulf airline will use Boeing Co 777-300 aircraft on the route, which will become its 15th US destination. Qatar Airways has remained bullish on the US even after Trump&#8217;s administration targeted the region with an attempt to block travel from six predominantly Muslim nations and a ban on carrying on laptop and tablet computers on flights from Middle East airports, including Doha. Emirates, which is based in Dubai, last week cited weaker demand caused by the restrictions as the cause for scaling back flights on five of its 12 US routes. \"We didn&#8217;t have massive declines like other carriers, so we still have robust loads to the US,\" said Qatar Airways CEO Akbar Al Baker, vowing not to reduce frequencies on US routes. \"President Trump is a very wise individual and a very good businessman, and I don&#8217;t think he will buy into bullying by the three American carriers.\" Al Baker said US planes are flying about 75% full, down about half a percentage point since the laptop ban. Qatar Airways is opening a total of 26 new routes this year and next.",
      "link": "http://themalaysianreserve.com/new/story/qatar-air-adds-san-francisco-after-emirates%E2%80%99-trump-cuts",
      "timestamp": 1493111475000
      },
      {
      "id": "2291865239957507",
      "title": "Major action on H1-B visa would worry India&#58; CEA",
      "media_displayName": "Free Press Journal",
      "img": "",
      "text": "Washington, Any &#8220;serious actions&#8221; taken by the Trump Administration on the H-1B visa programme would be a cause of &#8220;worry&#8221; as a majority of Indian exports in ...",
      "date": "2017-04-25 16:07:45+0800",
      "content": "Washington, Any \"serious actions\" taken by the Trump Administration on the H-1B visa programme would be a cause of \"worry\" as a majority of Indian exports in the services sector go to the US, Chief Economic Advisor Arvind Subramanian has said. \"If there are serious actions taken it&#8217;s something that&#8217;s going to kind of worry us a lot because, remember that our exports of services are about 40 to 45 per cent of the total exports,\" Subramanian told an audience here last week. \"Something like 50 to 60 per cent of all our exports of services goes to the United States. So it would worry us quite a bit,\" he said while responding to a question on the H1-B visa during his visit at the Peterson Institute, a top American economic think-tank. Subramanian said that India would be \"okay\" as long as the visa reforms are manageable and it&#8217;s keeping a close watch on it. \"Anything that is going to interfere with export growth creates anxiety (in India). In the context of the United States, we&#8217;re all watching very carefully the H1-B visa situation,\" the Chief Economic Advisor said. \"On the visa side, I don&#8217;t know but as long as you know we can keep this thing manageable. You know it should be okay,\" he said. During his US visit, Finance Minister Arun Jaitley had also taken up the the H-1B issue with the American side and highlighted the contribution Indian companies and professionals are making to the US economy. President Donald Trump had earlier this month signed an executive order for tightening the rules of the H-1B visa programme to stop its \"abuse\" and ensure that the visas are given to the \"most-skilled or highest paid\" petitioners, a decision that would impact India&#8217;s USD 150 billion IT industry. The Indian IT industry expressed serious concerns over this as these visas were mainly used by domestic IT professionals for short-term work in America. 1B is a non-immigrant visa that allows US companies to employ foreign workers in speciality occupations that require theoretical or technical expertise in specialised fields. Indian technology companies depend on it to hire tens of thousands of employees each year for their US operations. The US market accounts for about 60 per cent of the revenue of the Indian IT industry. 1B visa system was one of the major election promises of Trump. As per several US reports, a majority of the H-1B visas every year are grabbed by Indian IT professionals. India accounts for the highest pool of qualified IT professionals, whose services go a long way in making American companies globally competitive.",
      "link": "http://www.freepressjournal.in/india/major-action-on-h1-b-visa-would-worry-india-cea/1057544",
      "timestamp": 1493111265000
      },
      {
      "id": "2291865235763202",
      "title": "Value investments by Indian companies&#58; US on H-1B visa issue",
      "media_displayName": "Free Press Journal",
      "img": "",
      "text": "The US has said that it greatly values investments by Indian companies and wants to see bilateral business ties remain strong, days after Finance Minister Arun Jaitley raised the issue ...",
      "date": "2017-04-25 16:07:39+0800",
      "content": "The US has said that it greatly values investments by Indian companies and wants to see bilateral business ties remain strong, days after Finance Minister Arun Jaitley raised the issue of tightening of the H-1B visa regime with his American counterpart Steven Mnuchin. \"We want to see US-India business-to-business ties remain strong,\" State Department Acting Spokesman Mark Toner told reporters at his daily news conference. He was responding to questions on the ongoing review of H-1B visas by the Trump Administration and its impact on the Indian IT companies, who are heavily dependent on it. \"We greatly value Indian companies&#8217; continued investment in the US economy, which also, of course, supports thousands of US jobs,\" Toner said. \"With respect to any new requirements on visas, I&#8217;d have to check and see if that&#8217;s been updated,\" he said, adding that under the current government, the US has been looking at ways to strengthen the processes like visa interview and admission processes. Toner said that this had been the case from the beginning of the Trump administration and certainly with respect to immigration and with refugee flows. \"Those processes are ongoing,\" he said. On being asked about the visa review process, Toner said, \"It&#8217;s important to remember that this is always a part of how our consular bureau works and our consular officers work overseas, and our embassies and missions work overseas, and that is we&#8217;re always reviewing the processes that are in place to issue these visas and finding ways to strengthen them, because we want to ensure the security of the American people.\" During his US visit, Jaitley on Saturday had raised the H-1B visa issue with Treasury Secretary Mnuchin and highlighted the contribution Indian companies and professionals were making to the US economy. He had also raised the issue with US Commerce Secretary Wilbur Ross earlier. President Donald Trump had signed an executive order earlier this month for tightening the rules of the H-1B visa programme to stop its &#8216;abuse&#8217;, a decision that would impact India&#8217;s USD 150 billion IT industry. The Indian IT industry had expressed serious concerns over this as these visas were mainly used by domestic IT professionals for short-term work in America. 1B is a non-immigrant visa that allows US companies to employ foreign workers in speciality occupations that require theoretical or technical expertise in specialised fields. Indian technology companies depend on it to hire tens of thousands of employees each year for their US operations. The US market accounts for about 60 per cent of the revenue of the Indian IT industry. Reforming the H-1B visa system was one of the major election promises of Trump. As per several US reports, a majority of the H-1B visas every year are grabbed by Indian IT professionals. India accounts for the highest pool of qualified IT professionals, whose services go a long way in making American companies globally competitive. The US had earlier this month also accused top Indian IT firms TCS and Infosys of unfairly cornering the lion&#8217;s share of H-1B visas by putting extra tickets in the lottery system.",
      "link": "http://www.freepressjournal.in/world/value-investments-by-indian-companies-us-on-h-1b-visa-issue/1057546",
      "timestamp": 1493111259000
      },
      ],
      },
      
    };
  

  constructor() { 
    this.options = {
          barBackground: '#656a6f',
          gridBackground: '#e1e1e1',
          barWidth: '5',
          gridWidth: '2'
        };
  }

  ngOnInit() {

    this.splitDays1 = this.tgl1.split(',');
    this.splitDays2 = this.tgl2.split(',');
    this.splitDays3 = this.tgl3.split(',');

    this.hari1 = this.splitDays1;
    this.hari2 = this.splitDays2;
    this.hari3 = this.splitDays3;

    this.itemsDay1 = this.timeline["20170423"].items1;
    this.itemsDay2 = this.timeline["20170424"].items1;
    this.itemsDay3 = this.timeline["20170425"].items1;
    console.log(this.timeline["20170425"].items1);
    console.log(this.tgl2);
    console.log(this.splitDays1);
    console.log(this.hari1);
    
      this.dates = [];
      this.date = new Date();
      this.loadTimeline();
  }

  prevDay(event) {
    this.date = this.prevDate;
    this.loadTimeline();
    // console.log(this.date1 + this.date2 + this.date3)
  }

  nextDay(event) {
    this.date = this.nextDate;
    this.loadTimeline();
  }


  loadTimeline() {
    this.dates = [];
    this.prevDate = new Date(this.date);
    this.prevDate.setDate(this.date.getDate() - 1);
    this.nextDate = new Date(this.date);
    this.nextDate.setDate(this.date.getDate() + 1);

    this.items = [];
    for (var i = 2; i >= 0; i--) {
      var tempd = new Date(this.date);
      tempd.setDate(tempd.getDate() - i);
      var day = tempd.getDate();
      var monthIndex = tempd.getMonth();
      var year = tempd.getFullYear();
      if (i == 2) {
        this.date1 = year + ('0' + (monthIndex + 1)).slice(-2) + ('0' + day).slice(-2);
      }
      else if (i == 1) {
        this.date2 = year + ('0' + (monthIndex + 1)).slice(-2) + ('0' + day).slice(-2);
      }
      else {
        this.date3 = year + ('0' + (monthIndex + 1)).slice(-2) + ('0' + day).slice(-2);
      }
      this.items.push({ id: year + ('0' + (monthIndex + 1)).slice(-2) + ('0' + day).slice(-2), text: GlobalVariable.days[(tempd.getDay() == 0 ? 7 : tempd.getDay())] + ', ' + day + ' ' + GlobalVariable.months[monthIndex] });
      setTimeout(function () { }, 5000);
    }
    this.days = this.date1 + ',' + this.date2 + ',' + this.date3;

  }
}
