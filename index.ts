import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewTimelineComponent } from './src/new-timeline.component';
import { SlimScrollModule } from 'ng2-slimscroll';
export * from './src/new-timeline.component';

@NgModule({
  imports: [
    CommonModule,
    SlimScrollModule
  ],
  declarations: [
    BaseWidgetNewTimelineComponent
  
  ],
  exports: [
    BaseWidgetNewTimelineComponent,

 
  ]
})
export class TimelineModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TimelineModule,
      providers: []
    };
  }
}
